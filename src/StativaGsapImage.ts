import { html, css, LitElement } from 'lit';
import { property } from 'lit/decorators.js';
import gsap from 'gsap';
import ScrollTrigger from "gsap/ScrollTrigger";

export class StativaGsapImage extends LitElement {
  static styles = css`
    :host {
      display: block;
      color: var(--stativa-image-text-color, #000);
    }

    .image {
      width: 100%;
      height: auto;
    }

    img {
      width: 100%;
      height: 100%;
    }
  `;

  static timeline: GSAPTimeline;

  @property({ type: Number }) width = 0;

  @property({ type: Number }) height = 0;

  @property({ type: String }) src = "url";

  @property({ type: String }) alt = "alr";

  @property({ type: String }) title = "";

  @property({ type: String }) description = "";

  constructor() {
    super();

    gsap.registerPlugin(ScrollTrigger);
  }

  attributeChangedCallback() {
    StativaGsapImage.timeline.scrollTrigger?.refresh();
  }

  firstUpdated() {
    StativaGsapImage.timeline = gsap.timeline({
      scrollTrigger: {
        trigger: this.renderRoot.querySelector('.image'),
        markers: true,
        scrub: true,
        start: "top top",
        end: "bottom bottom",
    }});

    StativaGsapImage.timeline.fromTo(this.renderRoot.querySelector('.image-to-animate'), { opacity: '0' }, {opacity: 1 });
  }

  render() {
    return html`
      <div class="image">
        <h1>${this.title}</h1>
        <img class="image-to-animate" src=${this.src} alt=${this.alt} width=${this.width} height=${this.height} />
        <p>${this.description}</p>
      </div>
    `;
  }
}
